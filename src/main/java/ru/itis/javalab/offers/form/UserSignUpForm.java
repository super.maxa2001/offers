package ru.itis.javalab.offers.form;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.javalab.offers.validation.ValidPassword;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Email;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserSignUpForm {

    @Email(message = "{errors.incorrect.email}")
    private String email;
    private String phone;
    @ValidPassword(message = "{errors.incorrect.password}")
    private String password;
    private String repeatedPassword;
    private String kind;
}
