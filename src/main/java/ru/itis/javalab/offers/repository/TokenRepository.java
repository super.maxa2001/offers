package ru.itis.javalab.offers.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.javalab.offers.model.RefreshToken;
import ru.itis.javalab.offers.model.User;
import java.util.Optional;

public interface TokenRepository extends JpaRepository<RefreshToken, Long> {
    Optional<RefreshToken> findByRefreshToken(String token);
    void delete(RefreshToken refreshToken);
    Optional<RefreshToken> findByUser(User user);
}
