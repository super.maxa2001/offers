package ru.itis.javalab.offers.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.javalab.offers.model.Response;
import ru.itis.javalab.offers.service.TokenService;

@RestController
public class TokenController {

    @Autowired
    private TokenService tokenService;

    @PostMapping("/update")
    public ResponseEntity<Response> updateTokens(@RequestBody String token){
        return ResponseEntity.ok(tokenService.updateTokensWithRefresh(token));
    }
}
