package ru.itis.javalab.offers.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.javalab.offers.dto.UserDto;
import ru.itis.javalab.offers.form.UserSignInForm;
import ru.itis.javalab.offers.model.Response;
import ru.itis.javalab.offers.service.SignInService;

@RestController
public class SignInController {

    @Autowired
    private SignInService signInService;

    @PostMapping("/sign-in")
    public ResponseEntity<Response> signIn(@RequestBody UserSignInForm userSignInForm){
        return ResponseEntity.ok(signInService.signIn(userSignInForm));
    }
}
