package ru.itis.javalab.offers.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.javalab.offers.form.UserSignUpForm;
import ru.itis.javalab.offers.model.Response;
import ru.itis.javalab.offers.service.MailService;
import ru.itis.javalab.offers.service.SignUpService;

@RestController
public class SignUpController {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private SignUpService signUpService;

    @PostMapping("sign-up")
    public ResponseEntity<Response> signUp(@RequestBody UserSignUpForm userForm){
        return ResponseEntity.ok(signUpService.signUp(userForm));
    }

    @GetMapping("confirm")
    public ResponseEntity<Response> confirm(Long code){
        return ResponseEntity.ok(signUpService.makeConfirm(code));
    }
}
