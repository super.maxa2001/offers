package ru.itis.javalab.offers.security.token;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import ru.itis.javalab.offers.model.RefreshToken;
import ru.itis.javalab.offers.model.User;
import ru.itis.javalab.offers.security.details.UserDetailsImpl;
import ru.itis.javalab.offers.service.TokenService;

import java.util.Date;
import java.util.Optional;
import java.util.function.Supplier;

@Component
public class TokenAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private TokenService tokenService;

    @Autowired
    @Qualifier("UserDetailsServiceImpl")
    private UserDetailsService userDetailsService;

    @Value("${token.secret.key}")
    private String secretKey;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        TokenAuthentication tokenAuthentication = (TokenAuthentication) authentication;
        UserDetails userDetails;
        try {
            DecodedJWT decodedJWT = JWT.require(Algorithm.HMAC256(secretKey))
                    .build()
                    .verify(tokenAuthentication.getName());
            userDetails = new UserDetailsImpl(User.builder()
                    .email(decodedJWT.getClaim("email").asString())
                    .kind(User.Kind.valueOf(decodedJWT.getClaim("kind").asString()))
                    .build());
        }catch (JWTVerificationException e){
            throw new IllegalAccessError(e.getMessage());
        }
        String refreshToken = tokenAuthentication.getRefreshToken();
        if (refreshToken != null){
            Optional<RefreshToken> token = tokenService.getRefreshToken(refreshToken);
            if(!(token.isPresent() && token.get().getUser().getEmail().equals(userDetails.getUsername())
                    && token.get().getExpiredTime().compareTo(new Date()) > 0)){
                throw new IllegalAccessError("Refresh Token устарел. Войдите в систему заново, чтобы обновить свои токены!");
            }
        }
        authentication.setAuthenticated(true);
        tokenAuthentication.setUserDetails(userDetails);
        return tokenAuthentication;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return TokenAuthentication.class.equals(authentication);
    }


}
