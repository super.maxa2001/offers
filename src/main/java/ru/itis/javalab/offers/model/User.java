package ru.itis.javalab.offers.model;

import lombok.*;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "accounts")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String email;
    private String phone;
    private String hashPassword;
    private Boolean confirm;

    @Enumerated(value = EnumType.STRING)
    private Kind kind;

    public enum Kind{
        APPLICANT, COMPANY
    }
}
