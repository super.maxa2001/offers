package ru.itis.javalab.offers.mail;

public interface MailGenerator {
    String getMailForConfirm(String serverUrl, Long code);
}
