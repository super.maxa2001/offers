package ru.itis.javalab.offers.service;

import ru.itis.javalab.offers.dto.TokenDto;
import ru.itis.javalab.offers.form.UserSignInForm;
import ru.itis.javalab.offers.model.Response;

public interface SignInService {
    Response signIn(UserSignInForm userSignInForm);
}
