package ru.itis.javalab.offers.service;

import ru.itis.javalab.offers.form.UserSignUpForm;
import ru.itis.javalab.offers.model.Response;

public interface SignUpService {
    Response signUp(UserSignUpForm userForm);
    Response makeConfirm(Long code);
}
