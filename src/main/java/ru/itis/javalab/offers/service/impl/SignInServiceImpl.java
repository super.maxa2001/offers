package ru.itis.javalab.offers.service.impl;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.itis.javalab.offers.dto.TokenDto;
import ru.itis.javalab.offers.form.UserSignInForm;
import ru.itis.javalab.offers.model.Response;
import ru.itis.javalab.offers.model.User;
import ru.itis.javalab.offers.service.SignInService;
import ru.itis.javalab.offers.service.TokenService;
import ru.itis.javalab.offers.service.UsersService;

import java.util.function.Supplier;

@Service
public class SignInServiceImpl implements SignInService {

    @Autowired
    private UsersService usersService;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @SneakyThrows
    @Override
    public Response signIn(UserSignInForm userSignInForm) {
        User user = usersService.getUserByEmail(userSignInForm.getEmail());
        if (user == null) {
            return Response.builder()
                    .success(false)
                    .response("Пользователь с таким email не найден!")
                    .build();
        }
        if (user.getConfirm()) {
            if (passwordEncoder.matches(userSignInForm.getPassword(), user.getHashPassword())) {
                TokenDto tokens = tokenService.getNewTokens(user);
                return Response.builder()
                        .success(true)
                        .response(tokens)
                        .build();
            }
            return Response.builder()
                    .success(false)
                    .response("Неверный пароль!")
                    .build();
        }
        return Response.builder()
                .success(false)
                .response("Вы не можете пока войти на наш сайт. Пожалуйста, подвердите свою почту!")
                .build();
    }
}
