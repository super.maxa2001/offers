package ru.itis.javalab.offers.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.itis.javalab.offers.mail.MailGenerator;
import ru.itis.javalab.offers.mail.MailSender;
import ru.itis.javalab.offers.model.User;
import ru.itis.javalab.offers.service.MailService;
import ru.itis.javalab.offers.service.UsersService;

@Service
public class MailServiceImpl implements MailService {

    @Autowired
    private MailGenerator mailGenerator;

    @Autowired
    private MailSender mailSender;

    @Value("${server.url.confirm.mail}")
    private String serverUrl;

    @Value("${spring.mail.username}")
    private String from;

    @Autowired
    private UsersService usersService;

    @Override
    public void sendConfirmMail(User user) {
        Long id = usersService.getUserByEmail(user.getEmail()).getId();
        String confirmMail = mailGenerator.getMailForConfirm(serverUrl, id);
        mailSender.sendEmail(user.getEmail(), from, "Подтверждение почты", confirmMail);
    }
}
