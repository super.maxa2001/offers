package ru.itis.javalab.offers.service;

import ru.itis.javalab.offers.model.User;

public interface MailService {
    void sendConfirmMail(User user);
}
