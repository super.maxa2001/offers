package ru.itis.javalab.offers.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import ru.itis.javalab.offers.model.User;
import ru.itis.javalab.offers.repository.UsersRepository;
import ru.itis.javalab.offers.service.UsersService;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@Service
public class UsersServiceImpl implements UsersService {

    @Autowired
    private UsersRepository usersRepository;

    @Override
    public User getUserByEmail(String email) {
        Optional<User> opUser = usersRepository.findByEmail(email);
        return opUser.map(user -> User.builder()
                .email(user.getEmail())
                .confirm(user.getConfirm())
                .kind(user.getKind())
                .phone(user.getPhone())
                .hashPassword(user.getHashPassword())
                .id(user.getId())
                .build()).orElse(null);
    }

    @Override
    public void save(User user) {
        usersRepository.save(user);
    }

    @Override
    public User getUserById(Long code) {
        Assert.notNull(code);
        return usersRepository.findById(code).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public void delete(User oldUser) {
        usersRepository.delete(oldUser);
    }

    @Override
    public User getUserByPhone(String phone) {
        Optional<User> opUser = usersRepository.findByPhone(phone);
        return opUser.map(user -> User.builder()
                .email(user.getEmail())
                .confirm(user.getConfirm())
                .kind(user.getKind())
                .phone(user.getPhone())
                .hashPassword(user.getHashPassword())
                .id(user.getId())
                .build()).orElse(null);
    }
}
