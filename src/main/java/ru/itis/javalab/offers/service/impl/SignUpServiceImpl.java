package ru.itis.javalab.offers.service.impl;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.itis.javalab.offers.form.UserSignUpForm;
import ru.itis.javalab.offers.model.Response;
import ru.itis.javalab.offers.model.User;
import ru.itis.javalab.offers.service.MailService;
import ru.itis.javalab.offers.service.SignUpService;
import ru.itis.javalab.offers.service.UsersService;

@Service
public class SignUpServiceImpl implements SignUpService {

    @Autowired
    private UsersService usersService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private MailService mailService;

    @Override
    public Response signUp(UserSignUpForm userForm) {
        User.Kind kind;
        if (usersService.getUserByEmail(userForm.getEmail()) != null) {
            return Response.builder()
                    .success(false)
                    .response("Email, который вы ввели, на сайте уже используется. Введите другой email!")
                    .build();
        }
        if (usersService.getUserByPhone(userForm.getPhone()) != null) {
            return Response.builder()
                    .success(false)
                    .response("Номер телефона, который вы ввели, на сайте уже используется. Введите другой!")
                    .build();
        }
        if (userForm.getPassword().compareTo(userForm.getRepeatedPassword()) == 0) {
            if (userForm.getKind().compareTo("APPLICANT") == 0) {
                kind = User.Kind.APPLICANT;
            } else {
                kind = User.Kind.COMPANY;
            }
            User user = User.builder()
                    .email(userForm.getEmail())
                    .hashPassword(passwordEncoder.encode(userForm.getPassword()))
                    .phone(userForm.getPhone())
                    .confirm(false)
                    .kind(kind)
                    .build();
            usersService.save(user);
            mailService.sendConfirmMail(user);
            return Response.builder()
                    .success(true)
                    .response("Пользователь с адресом электронной почты " +
                            userForm.getEmail() +
                            " успешно зарегистрирован! Пожалуйста, перейдите на свою почту и подтвердите email.")
                    .build();
        }
        return Response.builder()
                .success(false)
                .response("Введенные пароли не совпадают. Пожалуйста, введите заново!")
                .build();
    }

    @SneakyThrows
    @Override
    public Response makeConfirm(Long code) {
        User user = usersService.getUserById(code);
        usersService.delete(user);
        user.setConfirm(true);
        usersService.save(user);
        return Response.builder()
                .success(true)
                .response("Вы подтвердили свою почту и теперь можете заходить на наш сайт!")
                .build();
    }
}
