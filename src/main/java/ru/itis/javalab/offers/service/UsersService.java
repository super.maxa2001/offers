package ru.itis.javalab.offers.service;

import ru.itis.javalab.offers.model.User;

import java.util.Optional;

public interface UsersService {
    User getUserByEmail(String email);
    void save(User user);
    User getUserById(Long code);

    void delete(User oldUser);

    User getUserByPhone(String phone);
}
