package ru.itis.javalab.offers.service;

import ru.itis.javalab.offers.dto.TokenDto;
import ru.itis.javalab.offers.model.RefreshToken;
import ru.itis.javalab.offers.model.Response;
import ru.itis.javalab.offers.model.User;

import java.util.Optional;

public interface TokenService {
    TokenDto getNewTokens(User user);
    Response updateTokensWithRefresh(String token);
    Optional<RefreshToken> getRefreshToken(String refreshToken);
}
